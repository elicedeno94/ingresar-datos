document.getElementById('formulario').addEventListener('submit', function(event) {
    event.preventDefault(); // Se evita que la página se recargue al enviar el formulario


    // Se obtiene los valores de los campos <3
    var nombres = document.getElementById('nombres').value;
    var apellidos = document.getElementById('apellidos').value;
    var fechaNacimiento = document.getElementById('fechaNacimiento').value;
    var cedula = document.getElementById('cedula').value;
    var telefono = document.getElementById('telefono').value;
    var correo = document.getElementById('correo').value;
    var estadoCivil = document.getElementById('estadoCivil').value;
    var direccion = document.getElementById('direccion').value;
    var nacionalidad = document.getElementById('nacionalidad').value;


       // Validacion del teléfono
     var telefonoValido = /^[0-9]+$/.test(telefono);

     if (!telefonoValido) {
         alert('El campo de teléfono solo debe contener números.');
         return; // Se detiene la ejecución si el teléfono no es válido
     }

    

    // Se muestra una alerta con los datos ingresados :) 
    alert(
        'Nombres: ' + nombres + '\n' +
        'Apellidos: ' + apellidos + '\n' +
        'Fecha de Nacimiento: ' + fechaNacimiento + '\n' +
        'Cédula: ' + cedula + '\n' +
        'Teléfono: ' + telefono + '\n' +
        'Correo Electrónico: ' + correo + '\n' +
        'Estado Civil: ' + estadoCivil + '\n' +
        'Dirección: ' + direccion + '\n' +
        'Nacionalidad: ' + nacionalidad + '\n' 
    );

    
});
